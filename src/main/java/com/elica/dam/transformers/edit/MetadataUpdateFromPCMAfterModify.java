package com.elica.dam.transformers.edit;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.artesia.asset.Asset;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.server.metadata.transformer.ValueTransformer;
import com.artesia.server.storage.StorageContext;

public class MetadataUpdateFromPCMAfterModify implements ValueTransformer{
	private static final Log log = LogFactory.getLog(MetadataUpdateFromPCMAfterModify.class);
	HashMap<Integer,MetadataField> metadataFields = new HashMap<Integer,MetadataField>(); 
	HashMap<Integer,String> modelsElica = new HashMap<Integer,String>(); 
	  
	private static final String MODEL_PATH = "fileproperties/model.properties";
	    private static final String METADATA_PATH = "fileproperties/metadata.properties";
	    private static final String LOOKUP_PATH = "fileproperties/lookup.properties";
	    private static final String PROJECT_PATH = "fileproperties/project.properties";
	    private static final String TABULAR_PATH = "fileproperties/tabular.properties";
	   
	    
	    protected static final Properties PROJECT_PROPERTIES = new Properties();
	    
	    static {
	        log.debug("MetadataUpdateFromPCMAfterModify: init of static block");
	        final ClassLoader classLoader = MetadataUpdateFromPCMAfterModify.class.getClassLoader();
	        try {
	            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(MODEL_PATH));
	            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(METADATA_PATH));
	            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(LOOKUP_PATH));
	            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(PROJECT_PATH));
	            PROJECT_PROPERTIES.load(classLoader.getResourceAsStream(TABULAR_PATH));
	            log.debug(PROJECT_PROPERTIES.stringPropertyNames());
	        } catch (IOException e) {
	            log.debug(e);
	        }	
	        log.debug("MetadataUpdateFromPCMAfterModify: end init of static block");
	    }

	@Override
	public void applyTransformation(List<MetadataElement> arg0, Asset[] arg1, StorageContext arg2) {
		// TODO Auto-generated method stub
		
		log.debug("Value Transformer for update PCM data "); 
		
		if (!arg0.get(0).getId().toString().equals("ELICA.PCM.TIPOLOGIA")) {
			String nameFile[]= new String [arg1.length];
			String model []=new String [arg1.length];
			String assetsId [] = new String [arg1.length];
			for (int i=0; i<arg1.length; i++) {
				assetsId [i]=arg1[i].getAssetId().toString();
				nameFile[i]=arg1[i].getName();
				model [i]=arg1[i].getMetadataModelId().toString();
						
			}
			UpdatePcmThread r = new UpdatePcmThread(PROJECT_PROPERTIES,assetsId,nameFile,model); 
			Thread t = new Thread(r); 
			t.start();
		}
		
		 
		 
	   
	
		 
	}
	
}
