package com.elica.dam.transformers.edit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Metadata {
	
	private String id [];
	private String type[]  ;
	private JSONObject value;
	private JSONObject valueArray[];
	private JSONArray metadataArray;
	private JSONObject jObjectMetadata;
	
	public Metadata(String id[], String type [],int index){
		this.setId(id);
		this.setType(type);
		this.setValueArray(new JSONObject[index]);
		this.setMetadataArray(new JSONArray());
		this.setJjObjectMetadata(new JSONObject());
	}

	public String [] getId() {
		return id; 
	}

	public void setId(String id []) {
		this.id = id;
	}

	public String[] getType() {
		return type;
	}

	public void setType(String type[] ) {
		this.type = type;
	}

	public JSONObject getValue() {
		return value;
	}

	public void setValue(JSONObject value) {
		this.value = value;
	}
	
	public void setValueJObject (String type, String value,int index) {
		JSONObject jObjectValue = new JSONObject();
		JSONObject jObjectValueExternal = new JSONObject();
		    try {
		 
		    		jObjectValue.put("type", type);
					jObjectValue.put("value", value);
				    jObjectValueExternal.put("value", jObjectValue);
				    setValueArrayElement(jObjectValueExternal,index);
				    setValueMetadataArray(index);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void setValueMetadataArray (int index) {
		
		JSONArray metadataArray= getMetadataArray();
		
	    
	    JSONObject jObjectMetadataValue = new JSONObject();
	    String id [] = getId();
	    String type[] = getType();
	    JSONObject value=getValueArray()[index];
	    		
	    try {

	    		jObjectMetadataValue.put("id", id[index] );
				jObjectMetadataValue.put("type",type[index]  );
				jObjectMetadataValue.put("value",value );
				metadataArray.put(jObjectMetadataValue);
				getJObjectMetadata().put("metadata", metadataArray);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public JSONArray getMetadataArray() {
		return metadataArray;
	}

	public void setMetadataArray(JSONArray metadataArray) {
		this.metadataArray = metadataArray;
	}
	
	public JSONObject getJObjectMetadata() {
		return jObjectMetadata;
	}

	public void setJjObjectMetadata(JSONObject jObjectMetadata) {
		this.jObjectMetadata = jObjectMetadata;
	}

	public JSONObject [] getValueArray() {
		return valueArray;
	}


	public void setValueArray(JSONObject valueArray[]) {
		this.valueArray = valueArray;
	}
	
	public void setValueArrayElement(JSONObject valueArray, int index) {
		getValueArray()[index] = valueArray;
	}
}
