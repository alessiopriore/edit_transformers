package com.elica.dam.transformers.edit;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.CascadingDomainValue;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataTable;
import com.artesia.metadata.MetadataTableField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;
import com.artesia.security.SecuritySession;
import com.artesiatech.meta.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.http.*;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.StringEntity;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;


public class DamCommon {
	private static final Log log = LogFactory.getLog(DamCommon.class);
	
	 public JSONObject getByModel(Properties elicaProperties, String model){
	        String token = getToken(elicaProperties);
	        DefaultHttpClient httpclient = new DefaultHttpClient();
	        JSONObject jObject= new JSONObject();
	        try {
	            HttpHost target = new HttpHost(elicaProperties.getProperty("host_URL"), Integer.parseInt(elicaProperties.getProperty("host_port")), elicaProperties.getProperty("host_scheme"));
	            HttpPost getRequest = new HttpPost(elicaProperties.getProperty("get_by_model"));
	            getRequest.setHeader(HttpHeaders.AUTHORIZATION, "Bearer "+token);
	            getRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json; utf-8");
	            StringEntity body = new StringEntity("{\t\"model\": \""+model+"\"}");
	            getRequest.setEntity(body);
	            HttpResponse httpResponse = httpclient.execute(target, getRequest);
	            HttpEntity entity = httpResponse.getEntity();
	            if (entity != null) {
	                String jsonResult = EntityUtils.toString(entity);
	                jObject = new JSONObject(jsonResult);
	                log.debug("Res:"+ jsonResult);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            httpclient.getConnectionManager().shutdown();
	            return jObject;
	        }
	    }
	    public JSONObject getByPRFAndCv(Properties elicaProperties, String PRF){
	        String token = getToken(elicaProperties);
	        DefaultHttpClient httpclient = new DefaultHttpClient();
	        JSONObject jObject= new JSONObject();
	        try {
	            HttpHost target = new HttpHost(elicaProperties.getProperty("host_URL"), Integer.parseInt(elicaProperties.getProperty("host_port")), elicaProperties.getProperty("host_scheme"));
	            HttpPost getRequest = new HttpPost(elicaProperties.getProperty("get_by_prf_path"));
	            getRequest.setHeader(HttpHeaders.AUTHORIZATION, "Bearer "+token);
	            getRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json; utf-8");
	            StringEntity body = new StringEntity("{\"prf\": \""+PRF+"\",\t\"catalogVersion\": \"Staged\"}");
	            getRequest.setEntity(body);
	            HttpResponse httpResponse = httpclient.execute(target, getRequest);
	            HttpEntity entity = httpResponse.getEntity();
	            
	            if (entity != null) {
	                String jsonResult = EntityUtils.toString(entity);
	                jObject = new JSONObject(jsonResult);
	                log.debug("Res:"+ jsonResult);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            httpclient.getConnectionManager().shutdown();
	            return jObject;
	        }
	    }
	  
	    private String  getToken (Properties elicaProperties){
	    	HttpClient httpclient = new DefaultHttpClient();
	        String token ="";
	        try {
	            // Setup the trustStore location and password usato solo per le chiamate a PCM da locale
	        	//per evitare di avere un eccezione SSL
	            /*System.setProperty("javax.net.ssl.trustStore","/src/main/resources/cacerts");
	        	System.setProperty("javax.net.ssl.trustStore","/src/main/resources/.keystore");
	        	System.setProperty("javax.net.ssl.trustStorePassword", "changeit");*/
 
	        	 // specify the host, protocol, and port
	            HttpHost target = new HttpHost(elicaProperties.getProperty("host_URL"), Integer.parseInt(elicaProperties.getProperty("host_port")), elicaProperties.getProperty("host_scheme"));
	            // specify the get request
	            HttpPost getRequest = new HttpPost(elicaProperties.getProperty("request_token_path"));  
	            String encoding= Base64.getEncoder().encodeToString(elicaProperties.getProperty("auth_credentials").getBytes());
	            getRequest.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
	            getRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
	            StringEntity body = new StringEntity("grant_type=client_credentials&scope=extended");
	            getRequest.setEntity(body);
	            log.debug("executing request to " + target);
	           
	            HttpResponse httpResponse = httpclient.execute(target, getRequest);
	            HttpEntity entity = httpResponse.getEntity();

	            if (entity != null) {
	                String jsonResult = EntityUtils.toString(entity);
	                JSONObject jObject = new JSONObject(jsonResult);
	                token = jObject.getString("access_token");
	                log.debug("Token:"+ token);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            log.debug("Exception: " + e.getMessage());
	        } finally {
	            httpclient.getConnectionManager().shutdown();
	            return token;
	        }
	    }	
	    
	    public String createSessionLogin(String url,Properties elicaProperties) {
			 String cookie = null;
			 try {
				 
				 HttpClient httpclient = HttpClients.createDefault();
				 HttpPost httppost = new HttpPost(url);

				 // Request parameters and other properties.
				 List<NameValuePair> params = new ArrayList<NameValuePair>(2);
				  
				 // Ambiente locale non legge dal file di properties 
				 /*params.add(new BasicNameValuePair("username","tsuper" ));
				  params.add(new BasicNameValuePair("password", "nimda"));*/
				 
				 params.add(new BasicNameValuePair("username",elicaProperties.getProperty("user")));
				 params.add(new BasicNameValuePair("password", elicaProperties.getProperty("password")));
				 httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
				 
				 //Execute and get the response.
				 HttpResponse response = httpclient.execute(httppost);
				 HttpEntity entity = response.getEntity();
				 if (entity != null) {
					    try (InputStream instream = entity.getContent()) {
					        // do something useful
					    	 String jsonResult = EntityUtils.toString(entity);
				            JSONObject jObject = new JSONObject(jsonResult);
				            log.debug("jsonResult of createSessionLogin: " + jsonResult);
				            String responseString = response.toString();
				            int startIndexCookie = responseString.indexOf("JSESSIONID");
				            int endIndexCookie = responseString.indexOf("path") - 2;
				            if (startIndexCookie != -1 && endIndexCookie != -1 ) {
				            	 cookie = responseString.substring(startIndexCookie,endIndexCookie);
				            	 log.debug("cookie of session: " + cookie);
				            }
				           	            
					    }
				 }

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
			
			return cookie; 
		}
		
		public void lockUnlockAsset(String action, String cookieSession,String url) throws ClientProtocolException, IOException, JSONException {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			
			log.debug("url lock unlock assets:  " + url);
			HttpPut httpPut = new HttpPut(url);
			httpPut.setHeader("Accept", "*/*");
			httpPut.setHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpPut.setHeader("Cookie", cookieSession);
			String encoding = Base64.getEncoder().encodeToString(("tsuper" + ":" + "!1%PrjtM@35S").getBytes());
			httpPut.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
			StringEntity body;
			try {
				body = new StringEntity("action=" + action);
				
				httpPut.setEntity(body);
				HttpResponse httpResponse = httpclient.execute(httpPut);
				HttpEntity entity = httpResponse.getEntity();
		        if (entity != null) {
		           String jsonResult = EntityUtils.toString(entity);
		           JSONObject jObject = new JSONObject(jsonResult);
		           log.debug("json lockUnlockAsset: " + jsonResult);
		       }
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

		
		public void updateMetadataPcm(JSONObject jObject, String cookieSession,String url) throws ClientProtocolException, IOException, JSONException {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			
			log.debug("url update:  " + url);
			HttpPatch httpPatch = new HttpPatch(url);
			httpPatch.setHeader("Accept", "*/*");
			httpPatch.setHeader("Content-type", "application/json; charset=UTF-8");
			httpPatch.setHeader("Cookie", cookieSession);
			String encoding = Base64.getEncoder().encodeToString(("tsuper" + ":" + "!1%PrjtM@35S").getBytes());
			httpPatch.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encoding);
			StringEntity body;
			try {
				body = new StringEntity(jObject.toString());
				
				httpPatch.setEntity(body);
				HttpResponse httpResponse = httpclient.execute(httpPatch);
				HttpEntity entity = httpResponse.getEntity();

		        if (entity != null) {
		           String jsonResult = EntityUtils.toString(entity);
		           JSONObject jObjectresult = new JSONObject(jsonResult);
		           log.debug("jsonResult update pcm metadata: " + jsonResult);
		       }
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		
		 public String [] populatePcmPrfMetadata(Properties elicaProperties,String PRF) {
	    
			String metadataContent[] = new String[20];
	        JSONObject result = getByPRFAndCv( elicaProperties, PRF);//Sostituire con metadataContent[0] PRF0114994A
	        log.debug("Prf da chiamare"+ PRF);
	        JSONObject data = new JSONObject();
	        try {
	            //TODO verificare se è vuoto saltare
	            data = new JSONObject(result.getString("data"));
	            log.debug("data populatePcmPrfMetadata: "+ data);
	            metadataContent[0] = data.getString("description").equalsIgnoreCase("null")?"":data.getString("description");
	            metadataContent[1] = ((data.getString("approvalStatus").equalsIgnoreCase("null"))?"":data.getString("approvalStatus"));
	            metadataContent[2] = data.getString("markets").equalsIgnoreCase("null")?"":data.getString("markets");
	            metadataContent[3] = data.getString("model").equalsIgnoreCase("null")?"":data.getString("model");
	            metadataContent[4] = data.getString("family").equalsIgnoreCase("null")?"":data.getString("family");
	            metadataContent[5] = data.getString("gamma").equalsIgnoreCase("null")?"":data.getString("gamma");
	            metadataContent[6] = data.getString("originalCategory").equalsIgnoreCase("null")?"":data.getString("originalCategory");
	            metadataContent[7] = data.getString("originalClass").equalsIgnoreCase("null")?"":data.getString("originalClass");
	            metadataContent[8] = data.getString("division").equalsIgnoreCase("null")?"":data.getString("division");
	            metadataContent[9] = data.getString("macrodivision").equalsIgnoreCase("null")?"":data.getString("macrodivision");
	            metadataContent[10] = data.getString("tipologia").equalsIgnoreCase("null")?"":data.getString("tipologia");
	            metadataContent[11] = data.getString("marchiopf").equalsIgnoreCase("null")?"":data.getString("marchiopf");
	            metadataContent[12] = data.getString("aspiranteFiltrante").equalsIgnoreCase("null")?"":data.getString("aspiranteFiltrante");
	            metadataContent[13] = data.getString("larghezza").equalsIgnoreCase("null")?"":data.getString("larghezza");
	            metadataContent[14] = data.getString("colore").equalsIgnoreCase("null")?"":data.getString("colore");
	            metadataContent[15] = data.getString("formatoCappa").equalsIgnoreCase("null")?"":data.getString("formatoCappa");
	            metadataContent[16] = data.getString("cicloVita").equalsIgnoreCase("null")?"":data.getString("cicloVita");
	            metadataContent[17] = data.getString("specialFinishing").equalsIgnoreCase("null")?"":data.getString("specialFinishing");
	            metadataContent[18] = data.getString("controls").equalsIgnoreCase("null")?"":data.getString("controls");
	            metadataContent[19] = data.getString("name").equalsIgnoreCase("null")?"":data.getString("name");
	            for (int i = 0; i < metadataContent.length; i++) {
	                log.debug("Content:"+metadataContent[i].toString());
	            }

	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        return metadataContent;
	    }


	    public String [] populatePcmModelMetadata( Properties elicaProperties, String model) {

	        JSONObject result = getByModel( elicaProperties,model);//Sostituire con metadataContent[0] PRF0114994A
	        String metadataContent[] = new String[14];
	        log.debug("Prf da chiamare"+ model);
	        JSONObject data = new JSONObject();
	        try {
	            data = new JSONObject(result.getString("data"));
	            metadataContent[0] = data.getString("model").equalsIgnoreCase("null")?"":data.getString("model");
	            metadataContent[1] = data.getString("family").equalsIgnoreCase("null")?"":data.getString("family");
	            metadataContent[2] = data.getString("gamma").equalsIgnoreCase("null")?"":data.getString("gamma");
	            metadataContent[3] = data.getString("originalCategory").equalsIgnoreCase("null")?"":data.getString("originalCategory");
	            metadataContent[4] = data.getString("originalClass").equalsIgnoreCase("null")?"":data.getString("originalClass");
	            metadataContent[5] = data.getString("division").equalsIgnoreCase("null")?"":data.getString("division");
	            metadataContent[6] = data.getString("macrodivision").equalsIgnoreCase("null")?"":data.getString("macrodivision");
	            metadataContent[7] = data.getString("tipologia").equalsIgnoreCase("null")?"":data.getString("tipologia");
	            metadataContent[8] = data.getString("marchiopf").equalsIgnoreCase("null")?"":data.getString("marchiopf");
	            log.debug("Marchiopfbool"+data.getString("marchiopf").equalsIgnoreCase("null"));
	            metadataContent[9] = data.getString("aspiranteFiltrante").equalsIgnoreCase("null")?"":data.getString("aspiranteFiltrante");
	            metadataContent[10] = data.getString("formatoCappa").equalsIgnoreCase("null")?"":data.getString("formatoCappa");
	            metadataContent[11] = data.getString("cicloVita").equalsIgnoreCase("null")?"":data.getString("cicloVita");
	            metadataContent[12] = data.getString("specialFinishing").equalsIgnoreCase("null")?"":data.getString("specialFinishing");
	            metadataContent[13] = data.getString("controls").equalsIgnoreCase("null")?"":data.getString("controls");
	            log.debug("Content:"+metadataContent.toString());
	        } catch (JSONException e) {
	            System.out.println(metadataContent[0]);
	            e.printStackTrace();
	        }
	       return metadataContent;
	    }
}
