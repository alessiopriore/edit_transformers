package com.elica.dam.transformers.edit;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdatePcmThread implements Runnable{
	private Properties elicaProperties;
	private String nameFile[];
	private String assetId[];
	private String model[];
	DamCommon damFunctionality = new DamCommon();
	
	public UpdatePcmThread(Properties projectProperties, String [] assetId,String [] nameFile,String [] model) {
		// TODO Auto-generated constructor stub
		this.elicaProperties=projectProperties;
		this.setAssetId(assetId);
		this.setNameFile(nameFile);
		this.setModel(model);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		final Log log = LogFactory.getLog(UpdatePcmThread.class);
		String metadataContent [] = new String[20];
		String token [];
		String idImagesProduct [] = {"ELICA.PCM.DESCRIZIONE","ELICA.PCM.STATUS","ELICA.PCM.MARKET","ELICA.PCM.MODEL","ELICA.PCM.FAMILY","ELICA.PCM.GAMMA",
				"ELICA.PCM.ORIGINAL.CATEGORY","ELICA.PCM.ORIGINAL.CLASS","ELICA.PCM.DIVISION","ELICA.PCM.MACRODIVISION","ELICA.PCM.TIPOLOGIA",
				"ELICA.PCM.MARCHIOPF","ELICA.PCM.ASPIRANTEFILTRANTE","ELICA.PCM.LARGHEZZA","ELICA.PCM.COLORE","ELICA.PCM.FORMATOCAPPA",
				"ELICA.PCM.CICLOVITA","ELICA.PCM.SPECIALFINSHING","ELICA.PCM.CONTROLS","ELICA.PCM.NAME",};
		String typeImagesProduct[] = {"com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField"
	    		 ,"com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField"
		,"com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField"};
		 String idDimensional [] = {"ELICA.PCM.MODEL","ELICA.PCM.FAMILY","ELICA.PCM.GAMMA","ELICA.PCM.ORIGINAL.CATEGORY","ELICA.PCM.ORIGINAL.CLASS",
				 "ELICA.PCM.DIVISION","ELICA.PCM.MACRODIVISION","ELICA.PCM.TIPOLOGIA","ELICA.PCM.MARCHIOPF","ELICA.PCM.ASPIRANTEFILTRANTE",
				 "ELICA.PCM.FORMATOCAPPA","ELICA.PCM.CICLOVITA","ELICA.PCM.SPECIALFINSHING","ELICA.PCM.CONTROLS"   };
	     String typeDimensional [] = {"com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField"
	    		 ,"com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField","com.artesia.metadata.MetadataField"};
	    
		for (int j =0; j< assetId.length; j++) {
			
			token = nameFile[j].split("_");
			
			/**/
			String urlsession =  elicaProperties.getProperty("urlsession");
		    String urlLock_unlock = elicaProperties.getProperty("urlLock_unlock_Update") + getAssetId()[j] +"/state";
		    String urlUpdate = elicaProperties.getProperty("urlLock_unlock_Update") + getAssetId()[j] ;
		    log.debug("urlsession####: " + urlsession);
		    log.debug("urlLock_unlock####: " + urlLock_unlock);
		    log.debug("urlUpdate####: " + urlUpdate);
			/*
			 *Ambiente locale
			 *
			 ****/
		    /*String urlsession =  "http://opentext-media-management-core-app:11090/otmmapi/v4/sessions/";
		    String urlLock_unlock = "http://opentext-media-management-core-app:11090/otmmapi/v4/assets/"+ getAssetId()[j] +"/state";
		    String urlUpdate = "http://opentext-media-management-core-app:11090/otmmapi/v4/assets/" + getAssetId()[j] ;
		    */
		    
		    Metadata metadata ;
		    String metadataPcm [];
		    if (model[j].equals("ELICA.MODEL.IMMAGINI.PRODOTTO")) {
				String tokenFirst [] = nameFile[j].split("#");
				log.debug("PRF: " + tokenFirst [0]);
				metadata = new Metadata(idImagesProduct,typeImagesProduct,idImagesProduct.length);
				metadataPcm = damFunctionality.populatePcmPrfMetadata(getElicaProperties(),tokenFirst [0]);
				 log.debug("metadataPcm size: " + metadataPcm.length);
			}else if(model[j].equals("ELICA.MODEL.DIMENSIONALE")) {
				log.debug("Name model: " + token[0]);
			    metadata = new Metadata(idDimensional,typeDimensional,idDimensional.length);
			    metadataPcm=damFunctionality.populatePcmModelMetadata(elicaProperties, token[0]);
			}else {
				break;
			}

			 String typevalue [] = {"string","string","string","string","string","string","string","string","string","string","string"
					 ,"string","string","string","string","string","string","string","string","string"};
	
			 for (int i=0; i<metadataPcm.length;i++) {
				 //log.debug("metadataPcm: " + metadataPcm[i]);
				 metadata.setValueJObject(typevalue[i], metadataPcm[i],i);
			 }
			 
			 JSONObject jObjectEdit= new JSONObject();
			 JSONObject jObjectData= new JSONObject();
			 try {
				    jObjectData.put("data", metadata.getJObjectMetadata());
				    jObjectEdit.put("edited_asset", jObjectData);
			 } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			 }
			
			 String cookie= damFunctionality.createSessionLogin(urlsession,elicaProperties);
			 try {
				 if (cookie != null) {
					 damFunctionality.lockUnlockAsset("lock",cookie, urlLock_unlock);
					 damFunctionality.updateMetadataPcm(jObjectEdit,cookie,urlUpdate);
					 damFunctionality.lockUnlockAsset("unlock",cookie,urlLock_unlock);
				 }
				 
			} catch (IOException | JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
	}
	public Properties getElicaProperties() {
		return elicaProperties;
	}
	public String[] getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId[]) {
		this.assetId = assetId;
	}
	public String[] getModel() {
		return model;
	}
	public void setModel(String model[]) {
		this.model = model;
	}
	public String[] getNameFile() {
		return nameFile;
	}
	public void setNameFile(String nameFile[]) {
		this.nameFile = nameFile;
	}
	

	 

}
